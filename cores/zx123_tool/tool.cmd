python3 zx123_tool.py -f -i flash_0.zxt -o flash.zxt -a Spectrum,../spectrum.zxt

python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,02,"ZX Next",../core02_zxnext.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,03,"TSConf",../core03_tsconf.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,04,"Atari 800XL",../core04_atari800.zxt
rem python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,05,"MSX1",../core05_msx1.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,05,"Bashkirija-2M",../core05_bashk2m.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,06,"Sega Master System",../core06_sms.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,07,"Radio-86RK",../core07_86rk.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,08,"NES",../core08_nes.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,09,"C64",../core09_c64.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,10,"Acorn Atom",../core10_atom.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,11,"Pravetz-8D",../core11_pravetz8d.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,12,"Amstrad CPC6128",../core12_cpc6128.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,13,"MSX2",../core13_msx2.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,14,"Next186Lite",../core14_next186lite.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,15,"SAM Coupe",../core15_samcoupe.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,16,"Apple2",../core16_apple2.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,17,"ColecoVision",../core17_colvision.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,18,"Orion-2010",../core18_orion2010.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,19,"Atari 2600",../core19_atari2600.zxt
python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a CORE,20,"BK-0010",../core20_bk0010.zxt

python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -a BIOS,../firmware.zxt

python3 zx123_tool.py -f -i flash.zxt -o flash.zxt -b 4 -m 2
copy flash.zxt "../flash.zxt"




