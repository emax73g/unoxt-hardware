python3 zx123_tool.py -f -i flash_32_0.zxt -o flash_32.revc.xt2 -a Spectrum,../xt2.revc/spectrum.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,02,"ZX Next",../xt2.revc/core02_zxnext.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,03,"TSConf",../xt2.revc/core03_tsconf.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,04,"Atari 800XL",../xt2.revc/core04_atari800.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,05,"MSX1",../xt2.revc/core05_msx1.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,06,"Sega Master System",../xt2.revc/core06_sms.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,07,"Radio-86RK",../xt2.revc/core07_86rk.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,08,"NES",../xt2.revc/core08_nes.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,09,"C64",../xt2.revc/core09_c64.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,10,"Acorn Atom",../xt2.revc/core10_atom.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,11,"Pravetz-8D",../xt2.revc/core11_pravetz8d.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,12,"Amstrad CPC6128",../xt2.revc/core12_cpc6128.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,13,"MSX2",../xt2.revc/core13_msx2.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,14,"Next186Lite",../xt2.revc/core14_next186lite.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,15,"SDRAM Test",../xt2.revc/corexx_sdram_test.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,16,"Apple2",../xt2.revc/core16_apple2.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,17,"ColecoVision",../xt2.revc/core17_colvision.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,18,"Orion-2010",../xt2.revc/core18_orion2010.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,19,"Atari 2600",../xt2.revc/core19_atari2600.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,20,"BK-0010",../xt2.revc/core20_bk0010.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a Special,../xt2.revc/special.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,21,"SRAM Test",../xt2.revc/corexx_sram_test.xt2
python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a CORE,22,"SDRAM Test",../xt2.revc/corexx_sdram_test.xt2

python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -a BIOS,../xt2.revc/firmware.xt2

python3 zx123_tool.py -f -i flash_32.revc.xt2 -o flash_32.revc.xt2 -b 4 -m 2
copy flash_32.revc.xt2 "../xt2.revc/flash_32.xt2"




