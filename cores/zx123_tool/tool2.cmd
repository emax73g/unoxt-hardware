python3 zx123_tool.py -f -i flash_0.zxt -o flash.xt2 -a Spectrum,../spectrum.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,02,"ZX Next",../core02_zxnext.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,03,"TSConf",../core03_tsconf.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,04,"Atari 800XL",../core04_atari800.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,05,"MSX1",../core05_msx1.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,06,"Sega Master System",../core06_sms.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,07,"Radio-86RK",../core07_86rk.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,08,"NES",../core08_nes.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,09,"C64",../core09_c64.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,10,"Acorn Atom",../core10_atom.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,11,"Pravetz-8D",../core11_pravetz8d.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,12,"Amstrad CPC6128",../core12_cpc6128.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,13,"MSX2",../core13_msx2.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,14,"Next186Lite",../core14_next186lite.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,15,"SAM Coupe",../core15_samcoupe.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,16,"Apple2",../core16_apple2.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,17,"ColecoVision",../core17_colvision.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,18,"Orion-2010",../core18_orion2010.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,19,"Atari 2600",../core19_atari2600.xt2
python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a CORE,20,"BK-0010",../core20_bk0010.xt2

python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -a BIOS,../firmware.xt2

python3 zx123_tool.py -f -i flash.xt2 -o flash.xt2 -b 4 -m 2
copy flash.xt2 "../flash.xt2"




