# UnoXT Cores

-----------------------------------
flash.zxt - Firmware for UnoXT 16MB
-----------------------------------
flash.xt2 - Firmware for UnoXT2 16MB
-----------------------------------

16MB Flash flashing from BIOS 

-----------------------------------
flash_32.zxt - Firmware for UnoXT 32MB
-----------------------------------
flash_32.xt2 - Firmware for UnoXT2 32MB
-----------------------------------

32MB Flash flashing from exp27 rooted ROM by commands from BIN directory

.upgrzxt for UnoXT and

.upgrxt2 for UnoXT2

-----------------------------------

Core binaries for flash at 

MX25L25645-08G SPI 256 Mbit Flash

for Spartan 6 SLX25, SRAM 4MB IS61WV204816BLL-10TLI,
 
for ZXDOS+ ecosystem

-----------------------------------

Empty Template for new cores developers

https://gitlab.com/emax73g/unoxt-empty-core-template

-----------------------------------

flash.zxt - 16MB Image for 32MB SPI flash

bin format - for Programmers or update from BIOS

-----------------------------------

spectrum.zxt exp27

Sources - https://gitlab.com/emax73g/unoxt-exp27-core

FAT32 SD Card ESXDOS 0.8.9 - http://www.esxdos.org/files/esxdos089.zip

------------------------------------

core02_zxnext.zxt ZX Next Core v.3.01.11

Sources - https://gitlab.com/emax73g/unoxt-zxnext-latest-core

FAT32 SD Card ZXNextOS - https://gitlab.com/thesmog358/tbblue

-------------------------------------

core03_tsconf.zxt TSConf

Sources - https://gitlab.com/emax73g/unoxt-tsconf-core

FAT32 SD Card - https://gitlab.com/emax73g/unoxt-tsconf-core/-/tree/main/SD

-------------------------------------

core04_atari800.zxt Atari 800XL

Sources - https://gitlab.com/emax73g/unoxt-atari800-core

FAT32 SD Card - /atari800/user/ *.atr, *.car, *xfd, *xex

-------------------------------------

core05_msx1.zxt MSX1

Sources - https://gitlab.com/emax73g/unoxt-msx1-core

FAT16 SD Card - https://gitlab.com/emax73g/unoxt-msx1-core/-/tree/main/Support/SD

-------------------------------------

core06_sms.zxt Sega Master System

Sources - https://gitlab.com/emax73g/unoxt-sms-core

Put *.sms ROMs to the SD Card

-------------------------------------

core07_86rk.zxt Rario-86RK

Sources - https://gitlab.com/emax73g/unoxt-86rk

Put *.rkr files to the FAT16 SD Card

Commands: U, dir, cd, filename.rkr

-------------------------------------

core08_nes.zxt Nintendo Entertainment System

Sources - https://gitlab.com/emax73g/unoxt-nes-core

Put *.nes ROMs to the SD Card

-------------------------------------

core09_c64.zxt UnoXT Commodore 64 Core

Sources - https://gitlab.com/emax73g/unoxt-c64-core

Put *.d64 Disk Images to the SD Card

Scroll Lock - toggle 15 KHz, 30 KHz HSYNC

F12 - OSD

F10 - Reset

F12/Load Disk/*.d64

LOAD"$",8

LIST

LOAD"filename*",8 or

LOAD"*",8

RUN

-------------------------------------

core10_atom.zxt UnoXT Acorn Atom Core

Sources - https://gitlab.com/emax73g/unoxt-acorn-atom-core

Keyboard:

SHIFT+10 - Menu

F10 - Reset

Turbo: F1 = 1Mhz, F2 = 2Mhz, F3 = 4Mhz, F4 = 8Mhz

SD Card FAT32 https://github.com/hoglet67/AtomSoftwareArchive/releases

-------------------------------------

core11_pravetz8d.zxt UnoXT Pravetz-8D Core

Sources - https://gitlab.com/emax73g/unoxt-pravetz-8d-core

Keyboard:

Scroll Lock - toggles vga/rgb+composite

End - NMI

Home+End - reset

Page up - image number up

Page down - image number down

-------------------------------------

core12_cpc6128.zxt Amstrad CPC6128

Sources - https://gitlab.com/emax73g/unoxt-amstrad-cpc6128-core/

FAT32 SD Card https://gitlab.com/emax73g/unoxt-amstrad-cpc6128-core/-/raw/main/SD_card.zip?inline=false

PageUp - toggle *.dsk

cat - Catalog

run"name - Run file

-------------------------------------

core13_msx2.zxt MSX1/2/2+/tR/3 Core

Sources - https://gitlab.com/emax73g/unoxt-msx2-core

FAT16 SD Card - https://gitlab.com/emax73g/unoxt-msx2-core/-/tree/main/sd

OCM-BIOS.DAT - need written first file in SD

Buttons - Hard Reset, Cold Reset, Warm Reset

F11 - Turbo

PrtScr - 15 kHz / 31 kHz HSYNC

-------------------------------------

core14_next186lite.zxt PC Next186Lite EGA/VGA Core

with AdLib Sound Card support

Sources - https://gitlab.com/emax73g/unoxt-pc-next186lite-core

SD Card - Write by Win32DiskImage hdd/HDD2MBRAM.img

Games compability list - https://docs.google.com/spreadsheets/d/1r07Ubfzquz2FxnKp4GROk8_Kq6kTlVW7YxVTmBDR_c4/edit#gid=0

https://docs.google.com/spreadsheets/d/1HS56y1Nzcfbs1rGCBEKRQbVnlDkuHW4d6jrbAG939mg/edit#gid=1282273667

-------------------------------------

core15_samcoupe.zxt SAM Coupe Core

Sources - https://github.com/emax73/unoxt-sam-coupe

F12 - disk menu

-------------------------------------

core16_apple2.zxt Apple ][+ Core

Sources - https://gitlab.com/emax73g/unoxt-apple2-core

F1-F10, Shift+F1-F10 - Select one of 20 *.nib images

combined into one *.img file, written from 0 sector of SD-Card

F12 - Soft Reset

-------------------------------------

core17_colvision.zxt ColecoVision Core

Sources - https://gitlab.com/emax73g/unoxt-colecovision-core

Put SD_Card.zip to FAT16  SD Card

--------------------------------------

core18_orion2010.zxt Orion-2010 Core

Sources - https://gitlab.com/emax73g/unoxt-orion-2010-core

2GB SD write image https://gitlab.com/emax73g/unoxt-orion-2010-core/-/raw/main/sd/orion2010sd2gb.img?inline=false

by https://www.alexpage.de/ USB Image Tool

F8 - toggle Monitors

--------------------------------------

core19_atari2600.zxt Atari 2600

Sources - https://gitlab.com/emax73g/unoxt-atari-2600-core

Esc - Enter to OSD

--------------------------------------

core20_bk0010.zxt BK-0010.01

Sources - https://gitlab.com/emax73g/unoxt-bk0010-core

Put /BK0010/m_basic.rom /BK0010/m_focal.rom to FAT16 SD Card

Run sequency - 
MO - enter to Monitor
SCROLL_LOCK 
ENTER - list of /BK0010 directory
begin enter of filename - TAB - completion 
S1000 - run from 1000 address

--------------------------------------

core05_bashk2m.zxt BASHKIRIA-2M Core for UnoXT Dev Board

https://gitlab.com/emax73g/unoxt-bashkirija-2m-core
from Evgeny7 http://bashkiria-2m.narod.ru/

SD Card need <= 4GB FAT16 Formatted 

ScrollLock - Reset


--------------------------------------

corexx_zxnext_3_01_08.zxt ZX Next Core v.3.01.08

Sources - https://gitlab.com/emax73g/uno-xt-next-core

FAT32 SD Card ZXNextOS - https://gitlab.com/emax73g/uno-xt-next-core/-/tree/unoxt.3.01.08/cores/zxnext/sd

-------------------------------------
