## Rev. D
Whats New
-----------------------
TapeIn - new schematic;

555 - bits color;

Pull Ups at PS2 and Joystick;

ESP-07 instread ESP-01;

EEPROM for saving settings;

and others small improvements.

-----------------------

## Rev.E - current low resistros at PS2

Added 1M tapein pulldown resistor that avoid noise at sound tract

Added 3V3 to test connector

-----------------------

## Rev.F - Level Shifter at PS2 

Level Shifter chip at PS2 connector instread resistors

-----------------------

## Rev.H - New footprints from UnoXT2 for comform soldering 

Footprints Quartz, SD, ESP-07 updated from UnoXT2 project

Ground polygons pushed off from ESP-07

Layer Stack now: Top, Gnd, Vdd, Bottom


