# UnoXT2 Second Generation of UnoXT Development Board

UnoXT2 is Second Generation of UnoXT Development Board with possibility running 20 Cores of Retro-Computers including legendary ZX Spectrum Next.

Spartan 6 SLX25, BGA256 chip, 25K cells,

SRAM 4MB,

SDRAM 64MB,

VGA, Audio Out with Volume control, SPDIF,  PS/2 Keyboard and Mouse, SD-CARD or uSD-CARD, DB9 Joystick, ESP-07 Wi-Fi Module.

Universal Power Supply 7V - 20V DC - Barrel 2.1x5.5mm 

Schematics - https://gitlab.com/emax73g/unoxt-hardware/-/blob/main/UnoXT2/docs/UnoXT2.Rev.D.pdf

Gerbers - https://gitlab.com/emax73g/unoxt-hardware/-/blob/main/UnoXT2/gerbers/UnoXT2.Rev.D.zip

BOM - https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/docs/UnoXT2.Rev.D.BOM.xls

Acrilic Enclosure - https://gitlab.com/emax73g/unoxt-hardware/-/blob/main/UnoXT2/enclosure/formulor.de/v3/UnoXT2.cover.v3.eps

3D Model -
<a href='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/docs/UnoXT2.Rev.D.top.3D.png' target='_blank'><img src='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/docs/UnoXT2.Rev.D.top.3D.png' border='0' alt='UnoXT2 3D Model'/></a>

<a href='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/docs/UnoXT2.Rev.D.bottom.3D.png' target='_blank'><img src='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/docs/UnoXT2.Rev.D.bottom.3D.png' border='0' alt='UnoXT2 3D Model'/></a>

## Have 20 Cores of Retro-Computers - 

ZX Spectrum EXP27

ZX Next

TSConf

Atari 800XL

MSX1

Sega Master System

Radio-86RK

NES

C64

Acorn Atom

Pravetz-8D

Amstrad CPC6128

MSX2

Next186Lite

SAM Coupe

Apple2

ColecoVision

Orion-2010

Atari 2600

BK-0010

As usual - Favorite Core - ZX Spectrum Next!!!

----------------

Firmware flash.xt2 -

https://gitlab.com/emax73g/unoxt-hardware/-/tree/main/cores

Empty Template for new cores developers

https://gitlab.com/emax73g/unoxt-empty-core-template

#

Based on UnoXT Rev.F Board - https://gitlab.com/emax73g/unoxt-hardware/-/tree/main/

Have more components and designed for Developers

needing more features that provided UnoXT Rev.F 

#

Based on Manuferhi ZXUNO VGA 2M project
https://github.com/ManuFerHi/ZXUNO_VGA_2M
and

ZX-UNO project by Superfo, Mcleod, Quest, AVillena and Hark0. http://zxuno.speccy.org/

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>

(CC BY-SA) License.
 