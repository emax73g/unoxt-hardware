# UnoXT 3D Printed Case from Freiwind
 
Walls 2 mm thick. Cutouts and holes for all connectors and protruding elements.
Stands for mounting the board and body halves with 4 mm holes for M3 fused nuts.
The halves are connected with M3x16(20) screws with a countersunk head.

<a href='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/enclosure/3dprinted/UnoXT-3.jpg?ref_type=heads' target='_blank'><img src='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/UnoXT2/enclosure/3dprinted/UnoXT-3.jpg?ref_type=heads' border='0' alt='UnoXT2 Enclosure Photo'/></a>