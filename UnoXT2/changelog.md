## UnoXT2 Rev. C
Whats New
-----------------------
Universal Power Supply 7V - 20V DC - Barrel 2.1x5.5mm 

SDRAM 64MB;

Audio Preamplifier with Volume Control;

SPDIF Output;

Extremelly precise RTC DS3232MZ with 256 Bytes RAM;

UART;

3 switches DIP;

Optional uSD Socket;

New PCB Routing;

-----------------------

## Rev.D - Noisly Audio PreAmplifyer removed from PCB 

Audio PreAmplifyer had removed - has now passive RC sound as in UnoXT

TEST Port had extended by 5V

SD DAT1, DAT2 connected with FPGA for 4-bit data mode. Added external 10K pull ups to SD

Holes for Power Switch decreased to 1.5 mm

Sound pins moved from E7, E6 => C9, B10. Rev.D need new versions of cores for sound!!!
