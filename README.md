# UnoXT ZX Spectrum Next Compatible Clone

# UnoXT2 Dev Board here - https://gitlab.com/emax73g/unoxt-hardware/-/tree/main/UnoXT2

UnoXT is ZX Uno modification Development Board with possibility running ZX Spectrum Next Compatible Core.

Spartan 6 SLX25, BGA256 chip, up 25K cells,

SRAM 4MB IS61WV204816BLL-10TLI,

Partial Cores supporting SRAM 1Mx16 IS61WV102416BLL-10TLI

VGA, Audio Out, PS/2, SD-CARD, DB9 Joystick, ESP-01 Wi-Fi Module.

Having ported ZX Spectrum Next Compatible Core -
https://gitlab.com/emax73g/uno-xt-next-core/-/tree/unoxt.3.01.08/cores/zxnext/flash

Sources of NextZXOS to FAT32 SD-Card -
https://gitlab.com/thesmog358/tbblue

Photo -
<a href='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/photos/20210911_135208.jpg' target='_blank'><img src='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/photos/20210911_135208.jpg' border='0' alt='UnoXT Photo'/></a>

<a href='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/photos/UnoXT.Rev.F.3D.png' target='_blank'><img src='https://gitlab.com/emax73g/unoxt-hardware/-/raw/main/photos/UnoXT.Rev.H.3D.png' border='0' alt='UnoXT 3D Model'/></a>

Empty Template for new cores developers

https://gitlab.com/emax73g/unoxt-empty-core-template

## Donations
UnoXT are Open Source and Free project. If you like this project and want to contribute to the development (software and hardware), or want to buy me a mug of beer -
please donate, the amount is up to you.
I am so happy to know how many users appreciate this project.

emax@cosmostat.com

Thanks!

PayPal

[![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/donate/?hosted_button_id=LEERSDCEANDXW)

BTC Address:

1Max73yEsKoSdxE79XKeebQF5FZWFTtLjE

#

Based on Manuferhi ZXUNO VGA 2M project
https://github.com/ManuFerHi/ZXUNO_VGA_2M
and

ZX-UNO project by Superfo, Mcleod, Quest, AVillena and Hark0. http://zxuno.speccy.org/

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>

(CC BY-SA) License.
 